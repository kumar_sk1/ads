package com.example.simplevideo.viewModel

import android.util.Log
import android.widget.ProgressBar
import com.example.simplevideo.model.MediaObject
import com.example.simplevideo.service.VastParserServices
import com.example.simplevideo.utils.Constants
import com.example.simplevideo.view.MainActivity
import com.example.simplevideo.view.PlayerActivity
import com.google.android.exoplayer2.ui.PlayerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.InputStream

class MediaObjectViewModel {
    private val TAG = MediaObjectViewModel::class.java.canonicalName
    private lateinit var mediaObjectList1: ArrayList<MediaObject>
    private lateinit var vastParserServices: VastParserServices
    private lateinit var playerActivity: PlayerActivity

    fun init(
        mainActivity: MainActivity,
        progressBar: ProgressBar,
        exoplayerView: PlayerView
    ) {
        Log.d(TAG, "init() Called")
        vastParserServices = VastParserServices()
        CoroutineScope(Dispatchers.IO).launch {
            val inp = getMediaStream(vastParserServices)
            if (inp != null) {
                mediaObjectList1 = vastParserServices.parseXMl(inp)
                CoroutineScope(Dispatchers.Main).launch {
                    playerActivity = PlayerActivity(mainActivity,progressBar,exoplayerView,mediaObjectList1)
                    playerActivity.initializePlayer()
                }
            }
        }


    }
    private suspend fun getMediaStream(vastParserServices: VastParserServices): InputStream? {
        Log.d(TAG, "getMediaStream() Called")
        return withContext(Dispatchers.IO) {
            vastParserServices.downloadUrl(Constants.BASE_URL)
        }
    }
}