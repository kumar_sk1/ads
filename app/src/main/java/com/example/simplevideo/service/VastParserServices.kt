package com.example.simplevideo.service

import android.util.Log
import com.example.simplevideo.model.MediaObject
import org.w3c.dom.Attr
import org.w3c.dom.CDATASection
import org.w3c.dom.Document
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

class VastParserServices {
    private val TAG = VastParserServices::class.java.canonicalName

    fun parseXMl(stream: InputStream): ArrayList<MediaObject> {
        Log.d(TAG, "parseXMl() Called")
        val mediaObjectList = ArrayList<MediaObject>()
        val dbf: DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
        val db: DocumentBuilder = dbf.newDocumentBuilder()
        val doc: Document = db.parse(stream)
        doc.documentElement.normalize()
        val mediaObject = doc.getElementsByTagName("MediaFile")
        for (i in 0 until mediaObject.length) {
            val attributesList = mediaObject.item(i).attributes
            val mediaUrlList = mediaObject.item(i).childNodes
            var bitrate = ""
            var mediaurl = ""
            for (j in 0 until attributesList.length) {
                if ((mediaObject.item(i).attributes.item(j) as Attr).name.equals("bitrate")) {
                    bitrate = (attributesList.item(j) as Attr).value
                    if (bitrate.toInt() <= 700) {
                        for (k in 0 until mediaUrlList.length) {
                            if (mediaUrlList.item(k) is CDATASection) {
                                mediaurl = (mediaUrlList.item(k) as CDATASection).data
                            }
                        }
                        mediaObjectList.add(MediaObject(mediaurl))
                    }
                }
            }
        }
        return mediaObjectList
    }

    // Given a string representation of a URL, sets up a connection and gets an input stream.
    @Throws(IOException::class)
    fun downloadUrl(urlString: String): InputStream? {
        Log.d(TAG, "downloadUrl() Called")
        val url = URL(urlString)
        return (url.openConnection() as? HttpURLConnection)?.run {
            readTimeout = 10000
            connectTimeout = 15000
            requestMethod = "GET"
            doInput = true
            // Starts the query
            connect()
            inputStream
        }
    }
}
