package com.example.simplevideo.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.example.simplevideo.R
import com.example.simplevideo.utils.NetworkUtils
import com.example.simplevideo.viewModel.MediaObjectViewModel
import com.google.android.exoplayer2.ui.PlayerView

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.canonicalName
    private lateinit var progressBar : ProgressBar
    private lateinit var exoplayerView : PlayerView
    private lateinit var mediaObjectViewModel: MediaObjectViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate() Called")
        super.onCreate(savedInstanceState)
        if (NetworkUtils.hasNetwork(this)) {
            setContentView(R.layout.activity_main)
            progressBar = findViewById(R.id.progressBar)
            exoplayerView = findViewById(R.id.exoplayerView)
            mediaObjectViewModel = MediaObjectViewModel()
            mediaObjectViewModel.init(this,progressBar,exoplayerView)
        } else {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
            setContentView(R.layout.no_internet_page)
        }
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBackPressed() Called")
        super.onBackPressed()
        finish()
    }

    override fun onStop() {
        Log.d(TAG, "onStop() Called")
        super.onStop()
        if (NetworkUtils.hasNetwork(this)) {
            exoplayerView.player?.release()
        }
    }

}
