package com.example.simplevideo.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.example.simplevideo.model.MediaObject
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory


class PlayerActivity(
    private val mainActivity: MainActivity,
    private val progressBar: ProgressBar,
    private val exoplayerView: PlayerView,
    private val mediaObjectList: ArrayList<MediaObject>
) : Player.EventListener {
    private val TAG = PlayerActivity::class.java.canonicalName
    private lateinit var simpleExoplayer: SimpleExoPlayer
    private var playbackPosition: Long = 0
    private var urlpos: Int = 0
    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(mainActivity, "exoplayer-sample")
    }

    fun initializePlayer() {
        Log.d(TAG, "initializePlayer() Called")
        simpleExoplayer = SimpleExoPlayer.Builder(mainActivity).build()
        if (mediaObjectList.size>0) {
            mediaObjectList[urlpos++].url?.let { preparePlayer(it) }
        }
        exoplayerView.player = simpleExoplayer
        simpleExoplayer.seekTo(playbackPosition)
        simpleExoplayer.playWhenReady = true
        simpleExoplayer.addListener(this)
    }

    private fun buildMediaSource(uri: Uri, type: String): MediaSource {
        Log.d(TAG, "buildMediaSource() Called")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)
    }

    private fun preparePlayer(videoUrl: String) {
        Log.d(TAG, "preparePlayer() Called")
        val uri = Uri.parse(videoUrl)
        val mediaSource = buildMediaSource(uri,"")
        simpleExoplayer.prepare(mediaSource)
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        Log.d(TAG, "onPlayerStateChanged() Called")
        when (playbackState) {
            Player.STATE_BUFFERING -> progressBar.visibility = View.VISIBLE
            Player.STATE_READY -> progressBar.visibility = View.INVISIBLE
            Player.STATE_ENDED ->
                if(urlpos < mediaObjectList.size) {
                    mediaObjectList[urlpos++].url?.let { preparePlayer(it)
                        Log.d("Suraj","$urlpos  $it")}
                } else {
                    watchAgain()
                }

        }
    }
    private fun watchAgain() {
        Log.d(TAG, "watchAgain() Called")
        val builder1: AlertDialog.Builder = AlertDialog.Builder(mainActivity)
        builder1.setMessage("Do you want to Play Again.")
        builder1.setCancelable(true)

        builder1.setNegativeButton(
            "No",
            DialogInterface.OnClickListener { dialog, id ->
                mainActivity.onBackPressed()
            }
        )

        builder1.setPositiveButton(
            "Yes",
            DialogInterface.OnClickListener { dialog, id ->
                urlpos = 0
                mediaObjectList[urlpos++].url?.let { it1 -> preparePlayer(it1) }
            }
        )
        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }


}